from enum import Enum, auto


class DamageType(Enum):
    THERMAL = auto()
    EM = auto()
    KINETIC = auto()
    EXPLOSIVE = auto()
