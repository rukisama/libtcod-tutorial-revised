import libtcodpy as libtcod
from entity_components.ai import BasicMonster, Turret


enemy_dict = {
    'orc': {
        'chance': 80,

        'fighter_component': {
            'hp': 20,
            'defense': 0,
            'power': 4,
            'xp': 35
        },

        'ai_component': BasicMonster,

        'entity_component': {
            'glyph': 'o',
            'color': libtcod.desaturated_green,
            'name': "Orc"
        }
    },

    'troll': {
        'chance': [[15, 3], [30, 5], [60, 7]],

        'fighter_component': {
            'hp': 30,
            'defense': 2,
            'power': 8,
            'xp': 100
        },

        'ai_component': BasicMonster,

        'entity_component': {
            'glyph': 'T',
            'color': libtcod.desaturated_green,
            'name': "Troll"
        }
    },

    'turret': {
        'chance': 50,

        'fighter_component': {
            'hp': 5,
            'defense': 2,
            'power': 2,
            'xp': 35
        },
        'ai_component': Turret,

        'entity_component': {
            'glyph': 'Y',
            'color': libtcod.darkest_grey,
            'name': "Turret"
        }
    }
}
