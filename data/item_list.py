import libtcodpy as libtcod
import item_functions
from game_messages import Message
from equipment_slots import EquipmentSlots
from data.damage_types import DamageType


# Item Format:
# 'item_id': {
#   'chance': [[]],
#   'item_component': {
#         'use_function': None,
#         'targeting': None,
#         'targeting_message': None,
#         'amount': None,
#         'damage': None,
#         'maximum_range': None,
#         'radius': None
#     },
#
#     'equippable_component': {
#         'slot': None,
#         'power_bonus': None,
#         'defense_bonus': None,
#         'max_hp_bonus': None
#     },
#
#     'entity_component': {
#         'name': None,
#         'glyph': None,
#         'color': None
#
#     }
# }

# Custom colors can be used: libtcod.Color(r, g, b) where r, g, and b are 0-255.

item_dict = {
    'healing_potion': {
        'chance': 35,
        'item_component': {
            'use_function': item_functions.heal,
            'amount': 40,
        },

        'equippable_component': None,

        'entity_component': {
            'name': "Healing Potion",
            'glyph': '!',
            'color': libtcod.light_violet

        }
    },
    'lightning_scroll': {
      'chance': [[25, 4]],
      'item_component': {
            'use_function': item_functions.cast_lightning,
            'damage': 40,
            'maximum_range': 5,
        },

      'equippable_component': None,

      'entity_component': {
            'name': "Lightning Scroll",
            'glyph': '#',
            'color': libtcod.yellow

        }
    },
    'fireball_scroll': {
      'chance': [[25, 6]],
      'item_component': {
            'use_function': item_functions.cast_fireball,
            'targeting': True,
            'targeting_message': Message("Left-click a target location, or right-click to cancel.", libtcod.light_cyan),
            'damage': 25,
            'radius': 3
        },

      'equippable_component': None,

      'entity_component': {
            'name': "Fireball Scroll",
            'glyph': '#',
            'color': libtcod.red

        }
    },

    'confusion_scroll': {
      'chance': [[10, 2]],
      'item_component': {
            'use_function': item_functions.cast_confuse,
            'targeting': True,
            'targeting_message': Message("Left-click a target location, or right-click to cancel.", libtcod.light_cyan),
            'amount': None,
            'damage': None,
            'maximum_range': 10,
            'radius': None
        },

      'equippable_component': None,

      'entity_component': {
            'name': "Confusion Scroll",
            'glyph': '#',
            'color': libtcod.pink

        }
    },

    'pistol': {
      'chance': [[50, 1]],
      'item_component': {
            'use_function': item_functions.fire_ranged,
            'targeting': True,
            'targeting_message': Message("Left-click a target location, or right-click to cancel.", libtcod.light_cyan),
            'amount': None,
            'damage': 10,
            'maximum_range': 10,
            'radius': None
        },

      'equippable_component': {
            'slot': EquipmentSlots.MAIN_HAND,
            'power_bonus': 0,
            'defense_bonus': 0,
            'max_hp_bonus': 0,
            'ranged': True
        },

      'entity_component': {
            'name': "Pistol",
            'glyph': chr(218),
            'color': libtcod.grey

        }
    },

    '9mm_round': {
        'chance': [[90, 1]],

        'item_component': {
            'use_function': None,
            'targeting': None,
            'targeting_message': None,
            'amount': None,
            'damage': None,
            'maximum_range': None,
            'radius': None
        },

        'equippable_component': None,

        'entity_component': {
            'name': "9mm round",
            'glyph': '`',
            'color': libtcod.darkest_grey
        }
    }

    # New items go here. Copy the format given above.

}

# Use None for unused components, rather than for their
# sub-components. Example:
#
# 'item_component': {
#         'use_function': "example",
#         'amount': 100,
#         'targeting': None,
#         'targeting_message': None
#     },
#     'equippable_component': None,
#     'entity_component': {
#         'name': "Example",
#         'glyph': '*',
#         'color': libtcod.white
#
#     }
