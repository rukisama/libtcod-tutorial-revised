import libtcodpy as libtcod

from entity_components.equipment import Equipment
from entity_components.equippable import Equippable
from entity_components.fighter import Fighter
from entity_components.inventory import Inventory
from entity_components.level import Level

from entity import Entity

from equipment_slots import EquipmentSlots

from game_messages import MessageLog

from game_states import GameStates

from galaxy_objects.location import Location

from render_functions import RenderOrder


def get_constants():
    window_title = 'Roguelike Tutorial Revised'

    fps_limit = 30

    screen_width = 96
    screen_height = 54
    viewport_width = 76
    viewport_height = 44
    status_width = screen_width - viewport_width
    status_height = screen_height
    info_width = status_width - 2
    info_height = 15
    camera_width = viewport_width
    camera_height = viewport_height

    bar_width = 16
    panel_height = 10
    panel_width = screen_width - status_width
    panel_y = screen_height - panel_height

    message_x = 2  # bar_width + 2
    message_width = screen_width - bar_width - 2
    message_height = panel_height - 2

    map_width = 150
    map_height = 75

    room_max_size = 10
    room_min_size = 6
    max_rooms = 50

    fov_algorithm = 0
    fov_light_walls = True
    fov_radius = 10

    max_monsters_per_room = 3
    max_items_per_room = 2

    colors = {
        'dark_wall':            libtcod.Color(0, 0, 100),
        'dark_ground':          libtcod.Color(50, 50, 150),
        'light_wall':           libtcod.Color(130, 110, 50),
        'light_ground':         libtcod.Color(200, 180, 50),
        'red_wall':             libtcod.darker_red,
        'red_floor':            libtcod.dark_red,
        'blue_wall':            libtcod.dark_blue,
        'blue_floor':           libtcod.blue,
        'orange_wall':          libtcod.darkest_flame,
        'orange_floor':         libtcod.dark_flame,
        'blue_star':            libtcod.sky,
        'blue_white_star':      libtcod.cyan,
        'white_star':           libtcod.white,
        'yellow_star':          libtcod.yellow,
        'orange_star':          libtcod.orange,
        'red_star':             libtcod.flame
    }

    item_types = ['healing_potion', 'sword', 'shield', 'lightning_scroll', 'fireball_scroll', 'confusion_scroll',
                  'dagger']

    constants = {
        'window_title':             window_title,
        'fps_limit':                fps_limit,
        'screen_width':             screen_width,
        'screen_height':            screen_height,
        'viewport_width':           viewport_width,
        'viewport_height':          viewport_height,
        'status_width':             status_width,
        'status_height':            status_height,
        'info_width':               info_width,
        'info_height':              info_height,
        'camera_width':             camera_width,
        'camera_height':            camera_height,
        'bar_width':                bar_width,
        'panel_width':              panel_width,
        'panel_height':             panel_height,
        'panel_y':                  panel_y,
        'message_x':                message_x,
        'message_width':            message_width,
        'message_height':           message_height,
        'map_width':                map_width,
        'map_height':               map_height,
        'room_max_size':            room_max_size,
        'room_min_size':            room_min_size,
        'max_rooms':                max_rooms,
        'fov_algo':                 fov_algorithm,
        'fov_light_walls':          fov_light_walls,
        'fov_radius':               fov_radius,
        'max_monsters_per_room':    max_monsters_per_room,
        'max_items_per_room':       max_items_per_room,
        'wall':                     chr(178),
        'colors':                   colors,
        'item_types':               item_types
    }

    return constants


def get_game_variables(constants):
    fighter_component = Fighter(hp=100, defense=1000, power=2000)  # 100, 1, 2
    inventory_component = Inventory(26)
    level_component = Level()
    equipment_component = Equipment()
    player = Entity(0, 0, '@', libtcod.white, 'Player', blocks=True, block_sight=False, render_order=RenderOrder.ACTOR,
                    fighter=fighter_component, inventory=inventory_component, level=level_component,
                    equipment=equipment_component)

    equippable_component = Equippable(EquipmentSlots.MAIN_HAND, power_bonus=2, is_equipped=True)
    dagger = Entity(0, 0, '-', libtcod.sky, 'Dagger', equippable=equippable_component)
    player.inventory.add_item(dagger)
    player.equipment.toggle_equip(dagger)

    colors = constants['colors']

    location = Location("Test Dungeon", colors['orange_wall'], colors['orange_floor'], 10, constants['map_width'],
                        constants['map_height'])
    location.generate_maps(constants['max_rooms'], constants['room_min_size'], constants['room_max_size'], player)

    message_log = MessageLog(constants['message_x'], constants['message_width'], constants['message_height'])

    game_state = GameStates.PLAYERS_TURN

    return player, location, message_log, game_state
