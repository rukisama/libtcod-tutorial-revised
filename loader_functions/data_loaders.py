import os

import shelve


def save_game(player, location, current_map, message_log, game_state):
    with shelve.open('savegame', 'n') as data_file:
        data_file['player_index'] = current_map.entities.index(player)
        data_file['map_index'] = location.current_level
        data_file['location'] = location
        data_file['message_log'] = message_log
        data_file['game_state'] = game_state


def load_game():
    if not os.path.isfile('savegame.dat'):
        raise FileNotFoundError

    with shelve.open('savegame', 'r') as data_file:
        player_index = data_file['player_index']
        map_index = data_file['map_index']
        location = data_file['location']
        message_log = data_file['message_log']
        game_state = data_file['game_state']

    current_map = location.game_maps[map_index]

    player = current_map.entities[player_index]

    return player, current_map, location, message_log, game_state
