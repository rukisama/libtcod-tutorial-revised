

class Exosuit:
    def __init__(self):
        self.integrity = 0
        self.armor = 0
        self.power_core = 0
        self.computer = 0
        self.left_arm = 0
        self.right_arm = 0


class ExoPart:
    def __init__(self, name, slot_type):
        self.name = name
        self.slot_type = slot_type
