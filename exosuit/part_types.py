from enum import Enum, auto


class ExoPartType(Enum):
    ARM = auto()
    COMPUTER = auto()
    ARMOR_PLATE = auto()
    POWER_CORE = auto()