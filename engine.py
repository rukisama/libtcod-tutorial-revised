import libtcodpy as libtcod

from death_functions import kill_monster, kill_player
from entity import get_blocking_entities_at_location
from fov_functions import initialize_fov, recompute_fov, update_tile_fov
from game_messages import Message
from game_states import GameStates
from input_handlers import handle_keys, handle_mouse, handle_main_menu
from loader_functions.initialize_new_game import get_constants, get_game_variables
from loader_functions.data_loaders import load_game, save_game
from menus import main_menu, message_box
from render_functions import render_all, clear_all
from camera import Camera
from galaxy_objects.game_map import Destination


def main():
    constants = get_constants()  # TODO: refactor functions to take constant dict instead of individual entries, maybe.

    libtcod.console_set_custom_font('consolas10x10_gs_tc.png', libtcod.FONT_TYPE_GREYSCALE | libtcod.FONT_LAYOUT_TCOD)

    libtcod.console_init_root(constants['screen_width'], constants['screen_height'], constants['window_title'],
                              fullscreen=False, renderer=libtcod.RENDERER_SDL)

    libtcod.sys_set_fps(constants['fps_limit'])

    con = libtcod.console_new(constants['screen_width'], constants['screen_height'])
    panel = libtcod.console_new(constants['screen_width'], constants['panel_height'])
    status = libtcod.console_new(constants['status_width'], constants['status_height'])
    info = libtcod.console_new(constants['info_width'], constants['info_height'])

    player = None
    location = None
    message_log = None
    game_state = None

    show_main_menu = True
    show_load_error_message = False

    main_menu_background_image = libtcod.image_load('galaxy_bg.png')

    key = libtcod.Key()
    mouse = libtcod.Mouse()

    while not libtcod.console_is_window_closed():
        libtcod.sys_check_for_event(libtcod.EVENT_KEY_PRESS | libtcod.EVENT_MOUSE, key, mouse)

        if show_main_menu:
            main_menu(con, main_menu_background_image, constants['screen_width'],
                      constants['screen_height'], 40)

            if show_load_error_message:
                message_box(con, 'No save game to load', 50, constants['screen_width'], constants['screen_height'])

            libtcod.console_flush()

            action = handle_main_menu(key)

            new_game = action.get('new_game')
            load_save_game = action.get('load_game')
            exit_game = action.get('exit')

            if show_load_error_message and (new_game or load_save_game or exit_game):
                show_load_error_message = False
            elif new_game:
                player, location, message_log, game_state = get_game_variables(constants)
                game_state = GameStates.PLAYERS_TURN

                show_main_menu = False

            elif load_save_game:
                try:
                    player, current_map, location, message_log, game_state = load_game()
                    show_main_menu = False

                except FileNotFoundError:
                    show_load_error_message = True
            elif exit_game:
                break

        else:
            libtcod.console_clear(con)
            play_game(player, location, message_log, game_state, con, panel, status, info, constants)

            show_main_menu = True


def play_game(player, location, message_log, game_state, con, panel, status, info, constants):
    fov_recompute = True

    fov_map = initialize_fov(location.game_maps[location.current_level])

    key = libtcod.Key()
    mouse = libtcod.Mouse()

    previous_game_state = game_state

    targeting_item = None

    camera = Camera(constants['camera_width'], constants['camera_height'], 1, 1)
    current_map = location.game_maps[location.current_level]

    while not libtcod.console_is_window_closed():
        libtcod.sys_check_for_event(libtcod.EVENT_KEY_PRESS | libtcod.EVENT_MOUSE, key, mouse)

        if fov_recompute:
            recompute_fov(fov_map, player.x, player.y, constants['fov_radius'], constants['fov_light_walls'],
                          constants['fov_algo'])

        fov_recompute = render_all(con, panel, status, info, player, current_map, fov_map, fov_recompute, message_log,
                                   mouse, game_state, camera, constants)

        libtcod.console_flush()

        clear_all(con, current_map.entities, camera)

        action = handle_keys(key, game_state)
        mouse_action = handle_mouse(mouse, camera)

        move = action.get('move')
        wait = action.get('wait')
        fire_ranged = action.get('fire_ranged')
        pickup = action.get('pickup')
        show_inventory = action.get('show_inventory')
        drop_inventory = action.get('drop_inventory')
        inventory_index = action.get('inventory_index')
        take_stairs = action.get('take_stairs')
        level_up = action.get('level_up')
        show_character_screen = action.get('show_character_screen')
        exit_game = action.get('exit')
        fullscreen = action.get('fullscreen')

        left_click = mouse_action.get('left_click')
        right_click = mouse_action.get('right_click')

        player_turn_results = []

        if move and game_state == GameStates.PLAYERS_TURN:
            dx, dy = move
            destination_x = player.x + dx
            destination_y = player.y + dy
            if not current_map.is_blocked(destination_x, destination_y):
                target = get_blocking_entities_at_location(current_map.entities, destination_x, destination_y)

                if target and target.fighter:
                    attack_results = player.fighter.attack(target)
                    player_turn_results.extend(attack_results)
                elif target and target.door:
                    target.door.toggle_door()
                    fov_map = update_tile_fov(fov_map, target.x, target.y, target.blocks, target.block_sight)
                    fov_recompute = True
                else:
                    player.move(dx, dy)
                    fov_recompute = True

                game_state = GameStates.ENEMY_TURN

        elif wait:
            game_state = GameStates.ENEMY_TURN

        elif fire_ranged and game_state == GameStates.PLAYERS_TURN:
            if player.equipment.main_hand:
                if player.equipment.main_hand.equippable.ranged:
                    player_turn_results.extend(player.inventory.use((player.equipment.main_hand, 1),
                                                                    entities=current_map.entities, fov_map=fov_map,
                                                                    fired=True))

        elif pickup and game_state == GameStates.PLAYERS_TURN:
            for entity in current_map.entities:
                if entity.item and entity.x == player.x and entity.y == player.y:
                    pickup_results = player.inventory.add_item(entity)
                    player_turn_results.extend(pickup_results)

                    break
            else:
                message_log.add_message(Message('There is nothing here to pick up.', libtcod.yellow))

        if show_inventory:
            previous_game_state = game_state
            game_state = GameStates.SHOW_INVENTORY

        if drop_inventory:
            previous_game_state = game_state
            game_state = GameStates.DROP_INVENTORY

        if inventory_index is not None and previous_game_state != GameStates.PLAYER_DEAD and inventory_index < len(
                player.inventory.items):
            item = player.inventory.items[inventory_index]
            if game_state == GameStates.SHOW_INVENTORY:
                player_turn_results.extend(player.inventory.use(item, entities=current_map.entities, fov_map=fov_map))
            elif game_state == GameStates.DROP_INVENTORY:
                player_turn_results.extend(player.inventory.drop_item(item))

        if take_stairs and game_state == GameStates.PLAYERS_TURN:
            for entity in current_map.entities:
                if entity.stairs and entity.x == player.x and entity.y == player.y:
                    if entity.stairs.destination == Destination.DOWN:
                        location.next_level(player, message_log)
                        current_map = location.game_maps[location.current_level]
                        fov_map = initialize_fov(current_map)
                        fov_recompute = True
                        libtcod.console_clear(con)

                        break

                    if entity.stairs.destination == Destination.UP:
                        location.previous_level(player, message_log)
                        current_map = location.game_maps[location.current_level]
                        fov_map = initialize_fov(current_map)
                        fov_recompute = True
                        libtcod.console_clear(con)

                        break

            else:
                message_log.add_message(Message('There are no stairs here.', libtcod.yellow))

        if level_up:
            if level_up == 'hp':
                player.fighter.base_max_hp += 20
                player.fighter.hp += 20
            elif level_up == 'str':
                player.fighter.base_power += 1
            elif level_up == 'def':
                player.fighter.base_defense += 1

            game_state = previous_game_state

        if show_character_screen:
            previous_game_state = game_state
            game_state = GameStates.CHARACTER_SCREEN

        if game_state == GameStates.TARGETING:
            if left_click:
                target_x, target_y = left_click

                item_use_results = player.inventory.use(targeting_item, entities=current_map.entities, fov_map=fov_map,
                                                        target_x=target_x, target_y=target_y, fired=True)
                player_turn_results.extend(item_use_results)
            elif right_click:
                player_turn_results.append({'targeting_cancelled': True})

        if exit_game:
            if game_state in (GameStates.SHOW_INVENTORY, GameStates.DROP_INVENTORY, GameStates.CHARACTER_SCREEN):
                game_state = previous_game_state
            elif game_state == GameStates.TARGETING:
                player_turn_results.append({'targeting_cancelled': True})
            else:
                save_game(player, location, current_map, message_log, game_state)

                return True

        if fullscreen:
            libtcod.console_set_fullscreen(not libtcod.console_is_fullscreen())

        for player_turn_result in player_turn_results:
            message = player_turn_result.get('message')
            dead_entity = player_turn_result.get('dead')
            item_added = player_turn_result.get('item_added')
            item_consumed = player_turn_result.get('consumed')
            item_used = player_turn_result.get('item_used')
            item_dropped = player_turn_result.get('item_dropped')
            equip = player_turn_result.get('equip')
            targeting = player_turn_result.get('targeting')
            fired = player_turn_result.get('fired')
            targeting_cancelled = player_turn_result.get('targeting_cancelled')
            xp = player_turn_result.get('xp')

            if message:
                message_log.add_message(message)
            if targeting_cancelled:
                game_state = previous_game_state

                message_log.add_message(Message('Targeting cancelled.'))

            if dead_entity:
                if dead_entity == player:
                    message, game_state = kill_player(dead_entity)
                else:
                    message = kill_monster(dead_entity)

                message_log.add_message(message)

            if item_added:
                current_map.entities.remove(item_added)

                game_state = GameStates.ENEMY_TURN

            if item_consumed:
                game_state = GameStates.ENEMY_TURN

            if item_used:
                game_state = GameStates.ENEMY_TURN

            if targeting and not fired:
                previous_game_state = GameStates.PLAYERS_TURN
                game_state = GameStates.TARGETING

                targeting_item = targeting

                message_log.add_message(targeting_item[0].item.targeting_message)

            if item_dropped:
                current_map.entities.append(item_dropped)

                game_state = GameStates.ENEMY_TURN

            if equip:
                equip_results = player.equipment.toggle_equip(equip)

                for equip_result in equip_results:
                    equipped = equip_result.get('equipped')
                    dequipped = equip_result.get('dequipped')

                    if equipped:
                        message_log.add_message(Message(f'You equipped the {equipped.name}.'))
                    if dequipped:
                        message_log.add_message(Message(f'You dequipped the {dequipped.name}.'))

                game_state = GameStates.ENEMY_TURN

            if xp:
                leveled_up = player.level.add_xp(xp)
                message_log.add_message(Message(f'You gain {xp} experience points.'))

                if leveled_up:
                    message_log.add_message(Message(
                        f"Your skill in battle grows! You've reached level {player.level.current_level}!",
                        libtcod.yellow
                    ))
                    previous_game_state = game_state
                    game_state = GameStates.LEVEL_UP

        if game_state == GameStates.ENEMY_TURN:
            for entity in current_map.entities:
                if entity.ai:
                    enemy_turn_results = entity.ai.take_turn(player, fov_map, current_map)

                    for enemy_turn_result in enemy_turn_results:
                        message = enemy_turn_result.get('message')
                        dead_entity = enemy_turn_result.get('dead')

                        if message:
                            message_log.add_message(message)

                        if dead_entity:
                            if dead_entity == player:
                                message, game_state = kill_player(dead_entity)
                            else:
                                message = kill_monster(dead_entity)

                            message_log.add_message(message)

                            if game_state == GameStates.PLAYER_DEAD:
                                break

                    if game_state == GameStates.PLAYER_DEAD:
                        break

            else:

                game_state = GameStates.PLAYERS_TURN


if __name__ == "__main__":
    main()
