class Camera:
    def __init__(self, camera_width, camera_height, camera_x, camera_y):
        self.camera_width = camera_width
        self.camera_height = camera_height
        self.camera_x = camera_x
        self.camera_y = camera_y

    def move_camera(self, target_x, target_y, map_width, map_height):
        x = target_x - round(self.camera_width / 2)
        y = target_y - round(self.camera_height / 2)

        if x < -1:
            x = -1
        if y < -1:
            y = -1
        if x > map_width - self.camera_width:
            x = map_width - self.camera_width
        if y > map_height - self.camera_height:
            y = map_height - self.camera_height

        if x != self.camera_x or y != self.camera_y:
            self.camera_x = x
            self.camera_y = y
            return {'fov_recompute': True}

        return {}

    def to_camera_coordinates(self, x, y):
        x, y = x - self.camera_x, y - self.camera_y

        if x < 0 or y < 0 or x >= self.camera_width or y >= self.camera_height:
            return {}

        return {'camera_loc': (x, y)}
