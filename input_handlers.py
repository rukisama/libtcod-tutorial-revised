import libtcodpy as libtcod

from game_states import GameStates


def handle_keys(key, game_state):
    if game_state == GameStates.PLAYERS_TURN:
        return handle_player_turn_keys(key)
    elif game_state == GameStates.PLAYER_DEAD:
        return handle_player_dead_keys(key)
    elif game_state == GameStates.TARGETING:
        return handle_targeting_keys(key)
    elif game_state in (GameStates.SHOW_INVENTORY, GameStates.DROP_INVENTORY):
        return handle_inventory_keys(key)
    elif game_state == GameStates.LEVEL_UP:
        return handle_level_up_menu(key)
    elif game_state == GameStates.CHARACTER_SCREEN:
        return handle_character_screen(key)

    return {}


def handle_targeting_keys(key):
    if key.vk == libtcod.KEY_ESCAPE:
        return {'exit': True}

    return {}


def handle_inventory_keys(key):
    index = key.c - ord('a')

    if index >= 0:
        return {'inventory_index': index}

    if key.vk == libtcod.KEY_ENTER and key.lalt:
        return {'fullscreen': True}
    elif key.vk == libtcod.KEY_ESCAPE:
        return {'exit': True}

    return {}


def handle_main_menu(key):
    if key.vk == libtcod.KEY_CHAR and key.c == ord('a'):
        return {'new_game': True}
    elif key.vk == libtcod.KEY_CHAR and key.c == ord('b'):
        return {'load_game': True}
    elif key.vk == libtcod.KEY_CHAR and key.c == ord('c'):
        return {'exit': True}

    return {}


def handle_level_up_menu(key):
    if key:
        if key.vk == libtcod.KEY_CHAR and key.c == ord('a'):
            return {'level_up': 'hp'}
        elif key.vk == libtcod.KEY_CHAR and key.c == ord('b'):
            return {'level_up': 'str'}
        elif key.vk == libtcod.KEY_CHAR and key.c == ord('c'):
            return {'level_up': 'def'}

    return {}


def handle_character_screen(key):
    if key.vk == libtcod.KEY_ESCAPE:
        return {'exit': True}

    return {}


def handle_player_dead_keys(key):
    if key.vk == libtcod.KEY_CHAR and key.c == ord('i'):
        return {'show_inventory': True}

    if key.vk == libtcod.KEY_ENTER and key.lalt:
        return {'fullscreen': True}
    elif key.vk == libtcod.KEY_ESCAPE:
        return {'exit': True}

    return {}


def handle_mouse(mouse, camera):
    (x, y) = (mouse.cx, mouse.cy)
    (x, y) = (camera.camera_x + x, camera.camera_y + y)

    if mouse.lbutton_pressed:
        return {'left_click': (x, y)}

    elif mouse.rbutton_pressed:

        return {'right_click': (x, y)}

    return {}


def handle_player_turn_keys(key):

    # Movement keys
    if key.vk == libtcod.KEY_UP or (key.vk == libtcod.KEY_CHAR and key.c == ord('k')) or key.vk == libtcod.KEY_KP8:
        return {'move': (0, -1)}
    elif key.vk == libtcod.KEY_DOWN or (key.vk == libtcod.KEY_CHAR and key.c == ord('j')) or key.vk == libtcod.KEY_KP2:
        return {'move': (0, 1)}
    elif key.vk == libtcod.KEY_LEFT or (key.vk == libtcod.KEY_CHAR and key.c == ord('h')) or key.vk == libtcod.KEY_KP4:
        return {'move': (-1, 0)}
    elif key.vk == libtcod.KEY_RIGHT or (key.vk == libtcod.KEY_CHAR and key.c == ord('l')) or key.vk == libtcod.KEY_KP6:
        return {'move': (1, 0)}
    elif key.vk == libtcod.KEY_CHAR and key.c == ord('y') or key.vk == libtcod.KEY_KP7:
        return {'move': (-1, -1)}
    elif key.vk == libtcod.KEY_CHAR and key.c == ord('u') or key.vk == libtcod.KEY_KP9:
        return {'move': (1, -1)}
    elif key.vk == libtcod.KEY_CHAR and key.c == ord('b') or key.vk == libtcod.KEY_KP1:
        return {'move': (-1, 1)}
    elif key.vk == libtcod.KEY_CHAR and key.c == ord('n') or key.vk == libtcod.KEY_KP3:
        return {'move': (1, 1)}
    elif key.vk == libtcod.KEY_CHAR and key.c == ord('z'):
        return {'wait': True}

    if key.vk == libtcod.KEY_CHAR and key.c == ord('g'):
        return {'pickup': True}

    elif key.vk == libtcod.KEY_CHAR and key.c == ord('i'):
        return {'show_inventory': True}

    elif key.vk == libtcod.KEY_CHAR and key.c == ord('d'):
        return {'drop_inventory': True}

    elif key.vk == libtcod.KEY_CHAR and key.c == ord('.') and key.shift:
        return {'take_stairs': True}

    elif key.vk == libtcod.KEY_CHAR and key.c == ord('c'):
        return {'show_character_screen': True}

    if key.vk == libtcod.KEY_ENTER and key.lalt:
        return {'fullscreen': True}

    if key.vk == libtcod.KEY_SPACE:
        return {'fire_ranged': True}

    elif key.vk == libtcod.KEY_ESCAPE:
        return {'exit': True}

    return {}
