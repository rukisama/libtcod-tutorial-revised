My code I "wrote" following the [libtcod Tutorial Revised](http://rogueliketutorials.com/). I did also add
a scrolling camera that follows the player with help from the code at the [original python libtcod tutorial](http://www.roguebasin.com/index.php?title=Complete_Roguelike_Tutorial,_using_python%2Blibtcod,_extras_scrolling_code). 

I used Python version 3.6 while developing, so that would likely be the best version to use to run it.
[libtcod](https://bitbucket.org/libtcod/libtcod) version used: 1.6.5 

Thanks to everyone involved in these tutorials (Jotaf for the original, TStand90 for the revised), everyone over at reddit's /r/roguelikedev, and everyone on the roguelikes Discord #roguelikedev channel for being such great resources. 