from random import randint
from entity_components.item import Item
from entity_components.equippable import Equippable
from entity_components.fighter import Fighter
from entity import Entity
from render_functions import RenderOrder


def from_dungeon_level(table, dungeon_level):
    if type(table) is int:
        return table

    for (value, level) in reversed(table):
        if dungeon_level >= level:
            return value

    return 0


def random_choice_index(chances):
    random_chance = randint(1, sum(chances))

    running_sum = 0
    choice = 0
    for w in chances:
        running_sum += w

        if random_chance <= running_sum:
            return choice
        choice += 1


def random_item_from_dict(choice_dict, x, y, dungeon_level):

    chances = []
    choices = []

    for item_id, components in choice_dict.items():
        chance = components.get('chance')
        chance = from_dungeon_level(chance, dungeon_level)

        chances.append(chance)
        choices.append(item_id)

    item_choice = choices[random_choice_index(chances)]
    item = choice_dict[item_choice]

    has_item_component = item.get('item_component')
    has_equippable_component = item.get('equippable_component')
    entity_component = item.get('entity_component')

    if has_item_component:

        use_function = has_item_component.get('use_function', None)
        amount = has_item_component.get('amount', None)
        targeting = has_item_component.get('targeting', None)
        targeting_message = has_item_component.get('targeting_message', None)
        damage = has_item_component.get('damage', None)
        maximum_range = has_item_component.get('maximum_range', None)
        radius = has_item_component.get('radius', None)

        item_component = Item(use_function, targeting, targeting_message, amount=amount,
                              damage=damage, maximum_range=maximum_range, radius=radius)

    else:
        item_component = None

    if has_equippable_component:

        slot = has_equippable_component.get('slot', None)
        power_bonus = has_equippable_component.get('power_bonus', None)
        defense_bonus = has_equippable_component.get('defense_bonus', None)
        max_hp_bonus = has_equippable_component.get('max_hp_bonus', None)
        ranged = has_equippable_component.get('ranged', False)

        equippable_component = Equippable(slot, power_bonus, defense_bonus, max_hp_bonus, ranged)

    else:
        equippable_component = None

    name = entity_component.get('name')
    glyph = entity_component.get('glyph')
    color = entity_component.get('color')

    new_item = Entity(x, y, glyph, color, name, render_order=RenderOrder.ITEM,
                      item=item_component, equippable=equippable_component)

    return new_item


def random_enemy_from_dict(choice_dict, x, y, dungeon_level):
    chances = []
    choices = []

    for enemy_id, components in choice_dict.items():
        chance = components.get('chance')
        chance = from_dungeon_level(chance, dungeon_level)

        chances.append(chance)
        choices.append(enemy_id)

    enemy_choice = choices[random_choice_index(chances)]
    enemy = choice_dict[enemy_choice]

    fighter_component_data = enemy.get('fighter_component')
    ai_component = enemy.get('ai_component')()
    entity_component = enemy.get('entity_component')

    hp = fighter_component_data.get('hp', None)
    defense = fighter_component_data.get('defense', None)
    power = fighter_component_data.get('power', None)
    xp = fighter_component_data.get('xp', None)

    name = entity_component.get('name')
    glyph = entity_component.get('glyph')
    color = entity_component.get('color')

    fighter_component = Fighter(hp=hp, defense=defense, power=power, xp=xp)
    new_enemy = Entity(x, y, glyph, color, name, blocks=True, render_order=RenderOrder.ACTOR,
                       fighter=fighter_component, ai=ai_component)

    return new_enemy
