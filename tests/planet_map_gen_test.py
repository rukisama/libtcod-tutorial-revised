import libtcodpy as libtcod
from random import randint


SCREEN_WIDTH = 100
SCREEN_HEIGHT = 100

noise_zoom = 2
noise_octaves = 15
noise_gen = libtcod.noise_new(2, 0.5, 2.0)
libtcod.noise_set_type(noise_gen, libtcod.NOISE_PERLIN)

hm = libtcod.heightmap_new(SCREEN_WIDTH, SCREEN_HEIGHT)
hm1 = libtcod.heightmap_new(SCREEN_WIDTH, SCREEN_HEIGHT)
hm2 = libtcod.heightmap_new(SCREEN_WIDTH, SCREEN_HEIGHT)
hm3 = libtcod.heightmap_new(SCREEN_WIDTH, SCREEN_HEIGHT)

libtcod.heightmap_add_fbm(hm1, noise_gen, noise_zoom, noise_zoom, 0.0, 0.0, noise_octaves, 0.0, 1.0)
libtcod.heightmap_add_fbm(hm2, noise_gen, noise_zoom * 2, noise_zoom * 2, 0.0, 0.0, noise_octaves // 2, 0.0, 1.0)

libtcod.heightmap_multiply_hm(hm1, hm2, hm)

libtcod.heightmap_normalize(hm, 10, 155)

for i in range(randint(10, 20)):
    libtcod.heightmap_dig_hill(hm, randint(0, SCREEN_WIDTH), randint(0, SCREEN_HEIGHT), randint(1, 8),
                               randint(-10, 0))

libtcod.heightmap_clamp(hm, 0, 255)

libtcod.heightmap_rain_erosion(hm, 5000, 0.5, 0.3)

ruins = []
for num_ruins in range(randint(10, 30)):
    x = randint(0, SCREEN_WIDTH)
    y = randint(0, SCREEN_HEIGHT)

    ruins.append((x, y))

libtcod.console_set_custom_font('terminal10x10_gs_tc.png', libtcod.FONT_LAYOUT_TCOD | libtcod.FONT_TYPE_GREYSCALE)
libtcod.console_init_root(SCREEN_WIDTH, SCREEN_HEIGHT, "Mapgen Test", renderer=libtcod.RENDERER_OPENGL)

mouse = libtcod.Mouse()
key = libtcod.Key()

if __name__ == '__main__':
    while not libtcod.console_is_window_closed():

        libtcod.sys_check_for_event(libtcod.EVENT_KEY_PRESS | libtcod.EVENT_MOUSE, key, mouse)

        if key.vk == libtcod.KEY_ESCAPE:
            break
        elif key.vk == libtcod.KEY_SPACE:
            libtcod.heightmap_dig_hill(hm, randint(0, SCREEN_WIDTH), randint(0, SCREEN_HEIGHT), randint(0, 8), 0)

        if mouse.lbutton_pressed:
            print(libtcod.heightmap_get_value(hm, mouse.cx, mouse.cy))

        for y in range(SCREEN_HEIGHT):
            for x in range(SCREEN_WIDTH):
                c = int(libtcod.heightmap_get_value(hm, x, y))
                libtcod.console_put_char_ex(0, x, y, ' ', libtcod.black, libtcod.Color(100 + c, 50 + c, c))

        for ruin in ruins:
            x, y = ruin[0], ruin[1]
            libtcod.console_put_char(0, x, y, '?', libtcod.BKGND_NONE)

        libtcod.console_flush()

