from random import randint
from galaxy_objects.star import Star


class Galaxy:
    def __init__(self, w, h):
        self.width = w
        self.height = h
        self.star_systems = []

    def generate_galaxy(self, density=0.5):
        num_stars = int(self.width * self.height * density)
        star_list = []

        for i in range(num_stars):
            collide = False
            x = randint(0, self.width)
            y = randint(0, self.height)
            for old_star in star_list:
                if x == old_star.x and y == old_star.y:
                    collide = True

            if not collide:
                new_star = Star(x, y)
                new_star.generate_star()
                star_list.append(new_star)

        self.star_systems = star_list


