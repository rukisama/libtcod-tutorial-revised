from random import randint, uniform, choice
from enum import Enum
from math import sqrt
import libtcodpy as libtcod

# Upper limit on mass: 315x solar mass (1.98e30)


class SpectralClass(Enum):
    CLASS_O = 'O'
    CLASS_B = 'B'
    CLASS_A = 'A'
    CLASS_F = 'F'
    CLASS_G = 'G'
    CLASS_K = 'K'
    CLASS_M = 'M'


class StarSize(Enum):
    HYPERGIANT = 'hypergiant'
    SUPERGIANT = 'supergiant'
    BRIGHT_GIANT = 'bright giant'
    GIANT = 'giant'
    SUBGIANT = 'subgiant'
    DWARF = 'dwarf'


def get_random_spectral_class():
    r = randint(1, 99881)

    if r <= 76450:
        s_class = SpectralClass.CLASS_M
    elif r <= 88550:
        s_class = SpectralClass.CLASS_K
    elif r <= 96150:
        s_class = SpectralClass.CLASS_G
    elif r <= 99150:
        s_class = SpectralClass.CLASS_F
    elif r <= 99750:
        s_class = SpectralClass.CLASS_A
    elif r <= 99880:
        s_class = SpectralClass.CLASS_B
    else:  # r = 99881
        s_class = SpectralClass.CLASS_O

    return s_class


class Star:
    def __init__(self, x, y):
        self.name = ""
        self.x = x
        self.y = y
        self.size = None
        self.spectral_class = None
        self.bolometric_magnitude = None
        self.temperature = None
        self.luminosity = None
        self.mass = None
        self.radius = None
        self.color = None
        self.h_inner = None
        self.h_outer = None
        self.planets = []

    def generate_star(self):
        class_o_size_list = [StarSize.BRIGHT_GIANT, StarSize.SUPERGIANT, StarSize.HYPERGIANT]
        class_b_size_list = [StarSize.SUBGIANT, StarSize.GIANT, StarSize.BRIGHT_GIANT, StarSize.SUPERGIANT,
                             StarSize.HYPERGIANT]
        other_class_size_list = [StarSize.DWARF, StarSize.SUBGIANT, StarSize.GIANT, StarSize.BRIGHT_GIANT,
                                 StarSize.SUPERGIANT, StarSize.HYPERGIANT]

        self.spectral_class = get_random_spectral_class()

        if self.spectral_class == SpectralClass.CLASS_M:
            self.size = choice(other_class_size_list)
            self.luminosity = uniform(0.01, 0.08) * 3.828e26
            self.temperature = randint(2400, 3700)
            self.color = libtcod.flame
            self.mass = uniform(0.08, 0.45) * 1.98e30  # kg
            self.radius = round(uniform(0.03, 0.08) * 696392)  # km
            self.bolometric_magnitude = uniform(8, 18) - 2.0
            abs_lum = 10 ** ((self.bolometric_magnitude - 4.72) / -2.5)
            self.h_inner = sqrt(abs_lum / 1.1) * 500
            self.h_outer = sqrt(abs_lum / 0.53) * 500
        elif self.spectral_class == SpectralClass.CLASS_K:
            self.size = choice(other_class_size_list)
            self.luminosity = uniform(0.08, 0.6) * 3.828e26
            self.temperature = randint(3700, 5200)
            self.color = libtcod.orange
            self.mass = uniform(0.45, 0.8) * 1.98e30  # kg
            self.radius = round(uniform(0.7, 0.96) * 696392)  # km
            self.bolometric_magnitude = uniform(5, 7) - 0.8
            abs_lum = 10 ** ((self.bolometric_magnitude - 4.72) / -2.5)
            self.h_inner = sqrt(abs_lum / 1.1) * 500
            self.h_outer = sqrt(abs_lum / 0.53) * 500
        elif self.spectral_class == SpectralClass.CLASS_G:
            self.size = choice(other_class_size_list)
            self.luminosity = uniform(0.6, 1.5) * 3.828e26
            self.temperature = randint(5200, 6000)
            self.color = libtcod.yellow
            self.mass = uniform(0.8, 1.04) * 1.98e30  # kg
            self.radius = round(uniform(0.96, 1.15) * 696392)  # km
            self.bolometric_magnitude = uniform(4, 5) - 0.4
            abs_lum = 10 ** ((self.bolometric_magnitude - 4.72) / -2.5)
            self.h_inner = sqrt(abs_lum / 1.1) * 500
            self.h_outer = sqrt(abs_lum / 0.53) * 500
        elif self.spectral_class == SpectralClass.CLASS_F:
            self.size = choice(other_class_size_list)
            self.luminosity = uniform(1.5, 5) * 3.828e26
            self.temperature = randint(6000, 7500)
            self.color = libtcod.light_yellow
            self.mass = uniform(1.04, 1.4) * 1.98e30  # kg
            self.radius = round(uniform(1.15, 1.4) * 696392)  # km
            self.bolometric_magnitude = uniform(4, 5) - 0.4
            abs_lum = 10 ** ((self.bolometric_magnitude - 4.72) / -2.5)
            self.h_inner = sqrt(abs_lum / 1.1) * 500
            self.h_outer = sqrt(abs_lum / 0.53) * 500
        elif self.spectral_class == SpectralClass.CLASS_A:
            self.size = choice(other_class_size_list)
            self.luminosity = uniform(5, 25) * 3.828e26
            self.temperature = randint(7500, 10000)
            self.color = libtcod.light_blue
            self.mass = uniform(1.4, 2.1) * 1.98e30  # kg
            self.radius = round(uniform(1.4, 1.8) * 696392)  # km
            self.bolometric_magnitude = uniform(4, 5) - 0.4
            abs_lum = 10 ** ((self.bolometric_magnitude - 4.72) / -2.5)
            self.h_inner = sqrt(abs_lum / 1.1) * 500
            self.h_outer = sqrt(abs_lum / 0.53) * 500
        elif self.spectral_class == SpectralClass.CLASS_B:
            self.size = choice(class_b_size_list)
            self.luminosity = uniform(25, 30000) * 3.828e26
            self.temperature = randint(10000, 30000)
            self.color = libtcod.cyan
            self.mass = uniform(2.1, 16) * 1.98e30  # kg
            self.radius = round(uniform(1.8, 6.6) * 696392)  # km
            self.bolometric_magnitude = uniform(-10, 0) - 2.0
            abs_lum = 10 ** ((self.bolometric_magnitude - 4.72) / -2.5)
            self.h_inner = sqrt(abs_lum / 1.1) * 500
            self.h_outer = sqrt(abs_lum / 0.53) * 500
        else:  # Class O
            self.size = choice(class_o_size_list)
            self.luminosity = uniform(30000, 1000000) * 3.828e26
            self.temperature = randint(30000, 80000)
            self.color = libtcod.sky
            self.mass = uniform(16, 315) * 1.98e30  # kg
            self.radius = round(uniform(6.6, 1708) * 696392)  # km
            self.bolometric_magnitude = uniform(-10, 0) - 2.0
            self.h_inner = 0
            self.h_outer = 0


if __name__ == '__main__':
    num_gen = 0
    while True:
        my_star = Star(10, 10)
        my_star.generate_star()
        if my_star.spectral_class == SpectralClass.CLASS_O:
            break
        num_gen += 1

    print(f"Number generated: {num_gen}")
    print(my_star)

