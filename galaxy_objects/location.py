import libtcodpy as libtcod
from galaxy_objects.game_map import GameMap
from game_messages import Message


class Location:
    def __init__(self, name, fore_color, back_color, depth, map_width, map_height, game_maps=None, current_level=1):
        self.name = name
        self.fore_color = fore_color
        self.back_color = back_color
        self.depth = depth
        self.map_width = map_width
        self.map_height = map_height
        self.current_level = current_level

        self.game_maps = game_maps

    def generate_maps(self, max_rooms, room_min_size, room_max_size, player):

        map_dict = {d: GameMap(self.map_width, self.map_height, self.fore_color, self.back_color, dungeon_level=d) for
                    d in range(1, self.depth + 1)}

        map_dict[1].make_map(max_rooms, room_min_size, room_max_size, self.map_width, self.map_height, player, top=True)

        for d in range(2, self.depth - 1):
            map_dict[d].make_map(max_rooms, room_min_size, room_max_size, self.map_width, self.map_height)

        map_dict[self.depth].make_map(max_rooms, room_min_size, room_max_size, self.map_width, self.map_height,
                                      bottom=True)

        self.game_maps = map_dict

    def next_level(self, player, message_log):

        self.game_maps[self.current_level].last_x, self.game_maps[self.current_level].last_y = player.x, player.y

        self.game_maps[self.current_level].entities.remove(player)
        self.current_level += 1
        self.game_maps[self.current_level].entities.insert(0, player)

        # player.fighter.heal(player.fighter.max_hp // 2)

        if self.game_maps[self.current_level].start_x and self.game_maps[self.current_level].start_y:
            player.x = self.game_maps[self.current_level].start_x
            player.y = self.game_maps[self.current_level].start_y

        message_log.add_message(Message('You move down the stairs.', libtcod.light_violet))

    def previous_level(self, player, message_log):
        self.game_maps[self.current_level].entities.remove(player)
        self.current_level -= 1
        self.game_maps[self.current_level].entities.insert(0, player)

        player.x, player.y = self.game_maps[self.current_level].last_x, self.game_maps[self.current_level].last_y

        message_log.add_message(Message('You move up the stairs.', libtcod.light_violet))





