import libtcodpy as libtcod
from random import randint
from enum import Enum

from data.item_list import item_dict
from data.enemy_list import enemy_dict

from entity_components.stairs import Stairs
from entity_components.door import Door

from entity import Entity

from game_messages import Message

from galaxy_objects.tile import Tile
from galaxy_objects.rectangle import Rect

from random_utils import from_dungeon_level, random_item_from_dict, random_enemy_from_dict

from render_functions import RenderOrder


class Destination(Enum):
    UP = 0
    DOWN = 1


class GameMap:
    def __init__(self, width, height, fore_color, back_color, entities=None, dungeon_level=1):
        self.width = width
        self.height = height
        self.fore_color = fore_color
        self.back_color = back_color
        self.tiles = self.initialize_tiles()
        if entities:
            self.entities = entities
        else:
            self.entities = []

        self.dungeon_level = dungeon_level

        self.start_x = None
        self.start_y = None
        self.last_x = None
        self.last_y = None

    def initialize_tiles(self):
        # noinspection PyUnusedLocal
        tiles = [[Tile(True) for y in range(self.height)] for x in range(self.width)]

        return tiles

    def make_map(self, max_rooms, room_min_size, room_max_size, map_width, map_height, player=None, top=False,
                 bottom=False):
        rooms = []
        num_rooms = 0

        center_of_last_room_x = None
        center_of_last_room_y = None

        for r in range(max_rooms):
            w = randint(room_min_size, room_max_size)
            h = randint(room_min_size, room_max_size)

            x = randint(0, map_width - w - 1)
            y = randint(0, map_height - h - 1)

            new_room = Rect(x, y, w, h)

            for other_room in rooms:
                if new_room.intersect(other_room):
                    break
            else:
                # Runs if room doesn't intersect another
                self.create_room(new_room)

                new_x, new_y = new_room.center()

                center_of_last_room_x = new_x
                center_of_last_room_y = new_y

                if num_rooms == 0:
                    center_of_first_room_x = new_x
                    center_of_first_room_y = new_y
                    if player is not None:
                        player.x = new_x
                        player.y = new_y
                    else:
                        self.start_x = new_x
                        self.start_y = new_y
                else:
                    prev_x, prev_y = rooms[num_rooms - 1].center()

                    if randint(0, 1) == 1:
                        self.create_h_tunnel(prev_x, new_x, prev_y)
                        self.create_v_tunnel(prev_y, new_y, new_x)
                    else:
                        self.create_v_tunnel(prev_y, new_y, prev_x)
                        self.create_h_tunnel(prev_x, new_x, new_y)

                self.place_items(new_room)
                self.place_enemies(new_room)

                rooms.append(new_room)
                num_rooms += 1

        if not top:
            stairs_component = Stairs(Destination.UP)
            up_stairs = Entity(center_of_first_room_x, center_of_first_room_y, '<', libtcod.white, 'Stairs',
                               render_order=RenderOrder.STAIRS, stairs=stairs_component)
            self.entities.append(up_stairs)

        if not bottom:
            stairs_component = Stairs(Destination.DOWN)
            down_stairs = Entity(center_of_last_room_x, center_of_last_room_y, '>', libtcod.white, 'Stairs',
                                 render_order=RenderOrder.STAIRS, stairs=stairs_component)
            self.entities.append(down_stairs)
        if player:
            self.entities.insert(0, player)

        self.place_doors()

    def place_doors(self):
        for x in range(1, self.width - 1):
            for y in range(1, self.height - 1):
                if not self.tiles[x][y].blocked:
                    place_door = False
                    north = self.tiles[x][y - 1].blocked
                    northeast = self.tiles[x + 1][y - 1].blocked
                    east = self.tiles[x + 1][y].blocked
                    southeast = self.tiles[x + 1][y + 1].blocked
                    south = self.tiles[x][y + 1].blocked
                    southwest = self.tiles[x - 1][y + 1].blocked
                    west = self.tiles[x - 1][y].blocked
                    northwest = self.tiles[x - 1][y - 1].blocked

                    # To be clear, I hate this:
                    if northwest and west and east and northeast and not north and not southwest and not south and \
                            not southeast:
                        place_door = True
                    elif southwest and west and east and southeast and not south and not north and not northwest and \
                            not northeast:
                        place_door = True
                    elif north and south and northeast and southeast and not northwest and not southwest and not west \
                            and not east:
                        place_door = True
                    elif northwest and southwest and north and south and not west and not northeast and not southeast \
                            and not east:
                        place_door = True

                    if place_door and randint(0, 4) > 3:
                        door_component = Door()
                        new_door = Entity(x, y, '+', libtcod.white, "Door", blocks=True, block_sight=True,
                                          render_order=RenderOrder.ITEM, door=door_component)
                        self.entities.append(new_door)

    def create_room(self, room):
        for x in range(room.x1 + 1, room.x2):
            for y in range(room.y1 + 1, room.y2):
                self.tiles[x][y].blocked = False
                self.tiles[x][y].block_sight = False

    def create_h_tunnel(self, x1, x2, y):
        for x in range(min(x1, x2), max(x1, x2) + 1):
            self.tiles[x][y].blocked = False
            self.tiles[x][y].block_sight = False

    def create_v_tunnel(self, y1, y2, x):
        for y in range(min(y1, y2), max(y1, y2) + 1):
            self.tiles[x][y].blocked = False
            self.tiles[x][y].block_sight = False

    def place_items(self, room):
        max_items_per_room = from_dungeon_level([[1, 1], [2, 4]], self.dungeon_level)

        number_of_items = randint(0, max_items_per_room)

        for i in range(number_of_items):
            x = randint(room.x1 + 1, room.x2 - 1)
            y = randint(room.y1 + 1, room.y2 - 1)

            if not any([entity for entity in self.entities if entity.x == x and entity.y == y]):
                item_choice = random_item_from_dict(item_dict, x, y, self.dungeon_level)

                self.entities.append(item_choice)

    def place_enemies(self, room):
        max_enemies_per_room = from_dungeon_level([[2, 1], [3, 4], [5, 6]], self.dungeon_level)

        number_of_enemies = randint(0, max_enemies_per_room)

        for i in range(number_of_enemies):
            x = randint(room.x1 + 1, room.x2 - 1)
            y = randint(room.y1 + 1, room.y2 - 1)

            if not any([entity for entity in self.entities if entity.x == x and entity.y == y]):
                enemy_choice = random_enemy_from_dict(enemy_dict, x, y, self.dungeon_level)

                self.entities.append(enemy_choice)

    def is_blocked(self, x, y):
        if self.tiles[x][y].blocked:
            return True

        return False

    def next_floor(self, player, message_log, constants):
        self.dungeon_level += 1
        self.entities = [player]

        self.tiles = self.initialize_tiles()
        self.make_map(constants['max_rooms'], constants['room_min_size'], constants['room_max_size'],
                      constants['map_width'], constants['map_height'], player)

        player.fighter.heal(player.fighter.max_hp // 2)

        message_log.add_message(Message('You take a moment to rest and recover your strength.', libtcod.light_violet))

