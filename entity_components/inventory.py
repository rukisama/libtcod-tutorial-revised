import libtcodpy as libtcod

from game_messages import Message


# noinspection PyUnresolvedReferences
class Inventory:
    def __init__(self, capacity):
        self.capacity = capacity
        self.items = []

    def add_item(self, item):
        results = []

        added = False
        for i in range(len(self.items)):
            check_item = self.items[i][0]
            count = self.items[i][1]

            if item.name == check_item.name:
                count += 1
                self.items[i] = (item, count)
                results.append({
                    'item_added': item,
                    'message': Message(f"You picked up the {item.name}", libtcod.light_blue)
                })
                added = True

        if not added:
            self.items.append((item, 1))
            results.append({
                'item_added': item,
                'message': Message(f"You picked up the {item.name}", libtcod.light_blue)
            })

        return results

    def use(self, item_tuple, **kwargs):
        results = []

        item_entity = item_tuple[0]
        item_component = item_entity.item
        equippable_component = item_entity.equippable
        fired = kwargs.get('fired')

        if item_component.use_function is not None and equippable_component is None:  # Ranged single-use

            if item_component.targeting and not (kwargs.get('target_x') or kwargs.get('target_y')):
                results.append({'targeting': item_tuple})
            else:
                kwargs = {**item_component.function_kwargs, **kwargs}
                item_use_results = item_component.use_function(self.owner, **kwargs)

                for item_use_result in item_use_results:
                    if item_use_result.get('consumed'):
                        self.remove_item(item_tuple)

                results.extend(item_use_results)

        elif item_component.use_function is None and equippable_component is not None:  # Melee weapons

            results.append({'equip': item_entity})

        elif item_component.use_function is not None and equippable_component is not None:  # Ranged weapon w/ ammo
            if equippable_component.is_equipped and fired:
                ammo_item_entry = None
                for item_entry in self.items:
                    name = item_entry[0].name
                    if name == "9mm round":
                        ammo_item_entry = item_entry

                if item_component.targeting and not (kwargs.get('target_x') or kwargs.get('target_y')) and \
                        ammo_item_entry is not None:
                    results.append({'targeting': item_tuple})
                elif item_component.targeting and not (kwargs.get('target_x') or kwargs.get('target_y')) and \
                        ammo_item_entry is None:
                    results.append({'message': Message("You have no ammunition!", libtcod.red)})
                else:
                    kwargs = {**item_component.function_kwargs, **kwargs}
                    item_use_results = item_component.use_function(self.owner, **kwargs)

                    for item_use_result in item_use_results:
                        if item_use_result.get('consumed'):
                            self.remove_item(item_tuple)
                        if item_use_result.get('use_ammo'):
                            for item_entry in self.items:
                                name = item_entry[0].name
                                if name == "9mm round":
                                    self.remove_item(item_entry)

                    results.extend(item_use_results)

            else:

                results.append({'equip': item_entity})

        return results

    def remove_item(self, item_tuple):
        item = item_tuple[0]
        count = item_tuple[1]
        item_index = self.items.index(item_tuple)

        if count > 1:
            count -= 1

            self.items[item_index] = (item, count)
        else:
            del self.items[item_index]

    def drop_item(self, item_tuple):
        results = []

        item = item_tuple[0]
        count = item_tuple[1]

        if self.owner.equipment.main_hand == item or self.owner.equipment.off_hand == item:
            self.owner.equipment.toggle_equip(item)

        item.x = self.owner.x
        item.y = self.owner.y

        if count > 1:
            count -= 1
            index = self.items.index(item_tuple)
            self.items[index] = (item, count)
            results.append({'item_dropped': item, 'message': Message(f"You dropped the {item.name}", libtcod.yellow)})
        else:
            self.remove_item(item_tuple)
            results.append({'item_dropped': item, 'message': Message(f"You dropped the {item.name}", libtcod.yellow)})

        return results

