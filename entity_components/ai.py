import libtcodpy as libtcod

from random import randint

from game_messages import Message


class BasicMonster:
    # noinspection PyUnresolvedReferences
    def take_turn(self, target, fov_map, game_map):
        results = []
        monster = self.owner

        if libtcod.map_is_in_fov(fov_map, monster.x, monster.y):

            if monster.distance_to(target) >= 2:
                monster.move_astar(target, game_map)

            elif target.fighter.hp > 0:
                attack_results = monster.fighter.attack(target)
                results.extend(attack_results)

        return results


class ConfusedMonster:
    def __init__(self, previous_ai, number_of_turns=10):
        self.previous_ai = previous_ai
        self.number_of_turns = number_of_turns

    # noinspection PyUnresolvedReferences
    def take_turn(self, target, fov_map, game_map):
        results = []

        if self.number_of_turns > 0:
            random_x = self.owner.x + randint(0, 2) - 1
            random_y = self.owner.y + randint(0, 2) - 1

            if random_x != self.owner.x and random_y != self.owner.y:
                self.owner.move_towards(random_x, random_y, game_map)

            self.number_of_turns -= 1
        else:
            self.owner.ai = self.previous_ai
            results.append({'message': Message(f"The {self.owner.name} is no longer confused", libtcod.red)})

        return results


class Turret:
    def take_turn(self,  target, fov_map, game_map):
        results = []

        turret = self.owner

        if libtcod.map_is_in_fov(fov_map, turret.x, turret.y):
            if target.fighter.hp > 0:
                attack_results = turret.fighter.attack(target)
                results.extend(attack_results)

        return results
