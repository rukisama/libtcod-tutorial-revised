class Door:
    def __init__(self, open_glyph='/', closed_glyph='+', is_open=False):
        self.open = is_open
        self.closed_glyph = closed_glyph
        self.open_glyph = open_glyph

    def toggle_door(self):
        if self.open:
            self.owner.glyph = self.closed_glyph
            self.owner.blocks = True
            self.owner.block_sight = True
        else:
            self.owner.glyph = self.open_glyph
            self.owner.blocks = False
            self.owner.block_sight = False

        self.open = not self.open

