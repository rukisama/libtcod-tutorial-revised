import libtcodpy as libtcod

from enum import Enum, auto

from game_states import GameStates

from menus import character_screen, inventory_menu, level_up_menu


class RenderOrder(Enum):
    CORPSE = auto()
    ITEM = auto()
    STAIRS = auto()
    ACTOR = auto()


def get_names_under_mouse(mouse, entities, fov_map, camera):
    (x, y) = (mouse.cx, mouse.cy)
    (x, y) = (camera.camera_x + x, camera.camera_y + y)

    names = [entity.name for entity in entities
             if entity.x == x and entity.y == y and libtcod.map_is_in_fov(fov_map, entity.x, entity.y)]
    names = ', '.join(names)

    return names.capitalize()


def render_bar(status, x, y, total_width, name, value, maximum, bar_color, back_color):
    bar_width = int(float(value) / maximum * total_width)

    libtcod.console_set_default_background(status, back_color)
    libtcod.console_rect(status, x, y, total_width, 1, False, libtcod.BKGND_SCREEN)

    libtcod.console_set_default_background(status, bar_color)
    if bar_width > 0:
        libtcod.console_rect(status, x, y, bar_width, 1, False, libtcod.BKGND_SCREEN)

    libtcod.console_set_default_foreground(status, libtcod.white)
    libtcod.console_print_ex(status, int(x + total_width / 2), y, libtcod.BKGND_NONE, libtcod.CENTER,
                             f"{name}: {value}/{maximum}")


def render_all(con, panel, status, info, player, game_map, fov_map, fov_recompute, message_log, mouse,
               game_state, camera, constants):

    screen_width = constants['screen_width']
    screen_height = constants['screen_height']
    bar_width = constants['bar_width']
    panel_width = constants['panel_width']
    panel_height = constants['panel_height']
    panel_y = constants['panel_y']
    info_width = constants['info_width']
    info_height = constants['info_height']
    # colors = constants['colors']
    map_width = constants['map_width']
    map_height = constants['map_height']
    status_width = constants['status_width']
    status_height = constants['status_height']
    viewport_width = constants['viewport_width']
    viewport_height = constants['viewport_height']

    camera_result = camera.move_camera(player.x, player.y, map_width, map_height)
    do_fov_recompute = camera_result.get('fov_recompute')

    if fov_recompute:
        for y in range(1, camera.camera_height):  # game_map.height
            for x in range(1, camera.camera_width):  # game_map.width
                map_x, map_y = camera.camera_x + x, camera.camera_y + y
                visible = libtcod.map_is_in_fov(fov_map, map_x, map_y)
                wall = game_map.tiles[map_x][map_y].block_sight

                if visible:
                    if wall:
                        libtcod.console_put_char_ex(con, x, y, chr(178), game_map.fore_color,
                                                    game_map.back_color)
                    else:
                        libtcod.console_set_char_background(con, x, y, game_map.back_color, libtcod.BKGND_SET)
                        libtcod.console_put_char_ex(con, x, y, ' ', game_map.back_color, game_map.back_color)

                    game_map.tiles[map_x][map_y].explored = True

                elif game_map.tiles[map_x][map_y].explored:
                    if wall:
                        libtcod.console_put_char_ex(con, x, y, chr(178), game_map.fore_color * libtcod.grey,
                                                    game_map.back_color * libtcod.grey)
                    else:
                        libtcod.console_put_char_ex(con, x, y, ' ', game_map.back_color * libtcod.grey,
                                                    game_map.back_color * libtcod.grey)

                else:
                    libtcod.console_put_char_ex(con, x, y, ' ', libtcod.black, libtcod.black)

    else:
        pass

    entities_in_render_order = sorted(game_map.entities, key=lambda x: x.render_order.value)

    for entity in entities_in_render_order:
        draw_entity(con, entity, fov_map, game_map, camera)

    libtcod.console_set_default_foreground(con, libtcod.white)

    libtcod.console_set_default_background(panel, libtcod.black)
    libtcod.console_set_default_background(status, libtcod.black)
    libtcod.console_clear(status)
    libtcod.console_clear(panel)
    libtcod.console_clear(info)

    y = 1
    for message in message_log.messages:
        libtcod.console_set_default_foreground(panel, message.color)
        libtcod.console_print_ex(panel, message_log.x, y, libtcod.BKGND_NONE, libtcod.LEFT, message.text)
        y += 1

    render_bar(status, 2, 2, bar_width, 'HP', player.fighter.hp, player.fighter.max_hp,
               libtcod.light_red, libtcod.darker_red)
    render_bar(status, 2, 4, bar_width, 'XP', player.level.current_xp, player.level.experience_to_next_level,
               libtcod.light_blue, libtcod.darker_blue)
    libtcod.console_print_ex(status, 2, 3, libtcod.BKGND_NONE, libtcod.LEFT,
                             f'Dungeon Level: {game_map.dungeon_level}')
    libtcod.console_print_ex(status, 2, 5, libtcod.BKGND_NONE, libtcod.LEFT,
                             f'Player Level: {player.level.current_level}')

    libtcod.console_set_default_foreground(status, libtcod.light_grey)
    libtcod.console_print_ex(info, 1, 0, libtcod.BKGND_NONE, libtcod.LEFT,
                             get_names_under_mouse(mouse, game_map.entities, fov_map, camera))

    draw_status_frame(status, status_width, status_height)
    draw_message_frame(panel, panel_width, panel_height)
    draw_viewport_frame(con, viewport_width, viewport_height)
    libtcod.console_blit(con, 0, 0, screen_width, screen_height, 0, 0, 0)
    libtcod.console_blit(status, 0, 0, status_width, status_height, 0, screen_width - status_width, 0)
    libtcod.console_blit(panel, 0, 0, panel_width, panel_height, 0, 0, panel_y)
    libtcod.console_blit(info, 0, 0, info_width, info_height, 0, screen_width - status_width + 1, 17)

    if game_state in (GameStates.SHOW_INVENTORY, GameStates.DROP_INVENTORY):
        if game_state == GameStates.SHOW_INVENTORY:
            inventory_title = 'Press the key next to an item to use it, ESC to cancel.\n'
        else:
            inventory_title = 'Press the key next to an item to drop it, ESC to cancel\n'

        inventory_menu(con, inventory_title, player, 50, screen_width, screen_height)

    elif game_state == GameStates.LEVEL_UP:
        level_up_menu(con, 'Level Up! Choose a stat to raise:', player, 40, screen_width, screen_height)

    elif game_state == GameStates.CHARACTER_SCREEN:
        character_screen(player, 30, 10, screen_width, screen_height)

    if do_fov_recompute:
        fov_recompute = do_fov_recompute
    else:
        fov_recompute = False

    libtcod.console_flush()

    return fov_recompute


def clear_all(con, entities, camera):

    for entity in entities:
        clear_entity(con, entity, camera)


def draw_entity(con, entity, fov_map, game_map, camera):
    if libtcod.map_is_in_fov(fov_map, entity.x, entity.y) or \
            (entity.stairs and game_map.tiles[entity.x][entity.y].explored) or \
            (entity.door and game_map.tiles[entity.x][entity.y].explored):

        camera_loc_get = camera.to_camera_coordinates(entity.x, entity.y)
        camera_loc = camera_loc_get.get('camera_loc')
        if camera_loc:
            (x, y) = camera_loc

            libtcod.console_set_default_foreground(con, entity.color)
            libtcod.console_put_char(con, x, y, entity.glyph, libtcod.BKGND_NONE)


def clear_entity(con, entity, camera):
    camera_loc = camera.to_camera_coordinates(entity.x, entity.y).get('camera_loc')
    if camera_loc:
        (x, y) = camera_loc
        libtcod.console_put_char(con, x, y, ' ', libtcod.BKGND_NONE)


def draw_status_frame(status, status_width, status_height):
    # Draw corners

    libtcod.console_put_char_ex(status, 0, 0, chr(203), libtcod.grey, libtcod.black)
    libtcod.console_put_char_ex(status, 0, status_height - 1, chr(202), libtcod.grey, libtcod.black)
    libtcod.console_put_char_ex(status, status_width - 1, 0, chr(187), libtcod.grey, libtcod.black)
    libtcod.console_put_char_ex(status, status_width - 1, status_height - 1, chr(188),
                                libtcod.grey, libtcod.black)

    # Draw top, bottom, and middle lines
    for x in range(1, status_width - 1):
        libtcod.console_put_char_ex(status, x, 0, chr(205), libtcod.grey, libtcod.black)
        libtcod.console_put_char_ex(status, x, status_height - 1, chr(205), libtcod.grey, libtcod.black)
        libtcod.console_put_char_ex(status, x, 16, chr(205), libtcod.grey, libtcod.black)
        libtcod.console_put_char_ex(status, x, 32, chr(205), libtcod.grey, libtcod.black)

    # Draw left and right lines
    for y in range(1, status_height - 1):
        libtcod.console_put_char_ex(status, 0, y, chr(186), libtcod.grey, libtcod.black)
        libtcod.console_put_char_ex(status, status_width - 1, y, chr(186), libtcod.grey, libtcod.black)

    # Draw other corners on top of lines
    libtcod.console_put_char_ex(status, 0, status_height - 10, chr(185), libtcod.grey, libtcod.black)
    libtcod.console_put_char_ex(status, 0, 16, chr(204), libtcod.grey, libtcod.black)
    libtcod.console_put_char_ex(status, status_width - 1, 16, chr(185), libtcod.grey, libtcod.black)
    libtcod.console_put_char_ex(status, 0, 32, chr(204), libtcod.grey, libtcod.black)
    libtcod.console_put_char_ex(status, status_width - 1, 32, chr(185), libtcod.grey, libtcod.black)

    # Draw names
    libtcod.console_print_ex(status, 2, 0, libtcod.BKGND_NONE, libtcod.LEFT, "Status")
    libtcod.console_print_ex(status, 2, 16, libtcod.BKGND_NONE, libtcod.LEFT, "Info")
    libtcod.console_print_ex(status, 2, 32, libtcod.BKGND_NONE, libtcod.LEFT, "Map")


def draw_message_frame(panel, panel_width, panel_height):
    # Draw corners

    libtcod.console_put_char_ex(panel, 0, 0, chr(204), libtcod.grey, libtcod.black)
    libtcod.console_put_char_ex(panel, 0, panel_height - 1, chr(200), libtcod.grey, libtcod.black)

    # Draw top and bottom lines
    for x in range(1, panel_width):
        libtcod.console_put_char_ex(panel, x, 0, chr(205), libtcod.grey, libtcod.black)
        libtcod.console_put_char_ex(panel, x, panel_height - 1, chr(205), libtcod.grey, libtcod.black)

    # Draw left and right lines
    for y in range(1, panel_height - 1):
        libtcod.console_put_char_ex(panel, 0, y, chr(186), libtcod.grey, libtcod.black)

    # Draw name
    libtcod.console_set_default_foreground(panel, libtcod.grey)
    libtcod.console_print_ex(panel, 2, 0, libtcod.BKGND_NONE, libtcod.LEFT, "Log")


def draw_viewport_frame(con, viewport_width, viewport_height):
    # Draw corners

    libtcod.console_put_char_ex(con, 0, 0, chr(201), libtcod.grey, libtcod.black)

    # Draw top and bottom lines
    for x in range(1, viewport_width):
        libtcod.console_put_char_ex(con, x, 0, chr(205), libtcod.grey, libtcod.black)

    # Draw left and right lines
    for y in range(1, viewport_height):
        libtcod.console_put_char_ex(con, 0, y, chr(186), libtcod.grey, libtcod.black)